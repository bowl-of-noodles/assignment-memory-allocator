#define _DEFAULT_SOURCE
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define INITIAL_HEAP_SIZE 20000

extern void debug(const char *fmt, ...);

static inline struct block_header* get_block_by_addr(void* data) { //возвращает блок, в котором выделена память
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

static void allocate_pages_after_block(struct block_header *last_block) {
    void* test_addr = (uint8_t*) last_block + size_from_capacity(last_block->capacity).bytes;
    test_addr = (uint8_t*) (getpagesize() * ((size_t) test_addr / getpagesize() + (((size_t) test_addr % getpagesize()) > 0)));
    test_addr = map_pages(test_addr, 1024, MAP_FIXED_NOREPLACE);
}

void test1(struct block_header* first_block) {
  debug("Test 1\n");

  const size_t size = 1000;
  void *data = _malloc(size);
  if (data == NULL)
    err("Test failed as malloc didn't allocate memory");

  debug_heap(stdout, first_block);

  if (first_block->is_free != false || first_block->capacity.bytes != size)
    err("Test failed as is_free or capacity is invalid");

  debug("Test 1 is passed\n");
  _free(data);
}

void test2(struct block_header* first_block) {
  debug("Test 2\n");

  void *data1 = _malloc(1000);
  void *data2 = _malloc(1000);
  if (data1 == NULL || data2 == NULL)
    err("Test failed as malloc didn't allocate memory");

  _free(data1);

  debug_heap(stdout, first_block);

  struct block_header *data1_block = get_block_by_addr(data1);
  struct block_header *data2_block = get_block_by_addr(data2);

  if (data1_block->is_free == false)
    err("Test failed. Memery block isn't free");
  if (data2_block->is_free == true)
    err("ТTest failed. Memery block isn't free");

  debug("Test 2 is passed\n");
  _free(data1);
  _free(data2);
}

void test3(struct block_header* first_block) {
    debug("Test 3\n");

    const size_t size1 = 1000, size2 = 2000, size3 = 3000;
    void *data1 = _malloc(size1);
    void *data2 = _malloc(size2);
    void *data3 = _malloc(size3);

    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Test 3 failed: _malloc returned NULL\n");
    }
    
    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);

    struct block_header *data_block1 = get_block_by_addr(data1);
    struct block_header *data_block3 = get_block_by_addr(data3);

    if (!data_block1->is_free) {
        err("Test 3 failed as free block isn't free\n");
    }
    if (data_block3->is_free) {
        err("Test 3 failed as this block is free\n");
    }
    if (data_block1->capacity.bytes != size1 + size2 + offsetof(struct block_header, contents)) {
        err("Test 3 failed aas free blocks weren't connected\n");
    }
    debug("Test 3 is passed\n\n");

    _free(data3);
    _free(data1);
    _free(data2);
}

void test4(struct block_header *first_block){
    debug("Test 4\n");
    void *data1 = _malloc(INITIAL_HEAP_SIZE);
    void *data2 = _malloc(INITIAL_HEAP_SIZE + 600);
    void *data3 = _malloc(2500);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Test 4 failed as _malloc returned NULL\n");
    }
    _free(data3);
    _free(data2);

    debug_heap(stdout, first_block);

    struct block_header *data_block1 = get_block_by_addr(data1);
    struct block_header *data_block2 = get_block_by_addr(data2);

    //
    if ((uint8_t *)data_block1->contents + data_block1->capacity.bytes != (uint8_t*) data_block2){
        err("Test 4 failed: new region wasn't created after last\n");
    }
    _free(data1);
    _free(data2);
    _free(data3);

    debug("Check if free");
    debug_heap(stdout, first_block);

    debug("Test 4 passed\n");
}

void test5(struct block_header *first_block) {
    debug("Test 5\n");

    void *data1 = _malloc(10000);
    if (data1 == NULL) {
        err("Test 5 failed as malloc returned null\n");
    }

    struct block_header *addr = first_block;
    while (addr->next != NULL) addr = addr->next;
    allocate_pages_after_block(addr);
    void *data2 = _malloc(100000);

    debug_heap(stdout, first_block);

    struct block_header *data2_block = get_block_by_addr(data2);
    if (data2_block == addr) {
        err("Test 5 failed as block wasn't allocated at new place\n");
    }
    debug("Test 5 is passed\n");

    _free(data1);
    _free(data2);
}


int main(){
    debug("Heap initializing started\n");
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL) {
        err("Can't initialize heap");
    }
    debug("Heap initialized\n");
    struct block_header *first_block = (struct block_header*) heap;

    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    test5(first_block);

    debug("All tests are passed\n");

    return 0;
}



